<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite - Valid Account</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>603e7497-e145-4910-acee-c197615e51be</testSuiteGuid>
   <testCaseLink>
      <guid>5c6f19ce-a587-4063-9cdf-7e2a3a573217</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC01 - Login/Login - Valid Account</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>17374d8f-2e2c-4de3-8732-3cd6837baa65</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Valid Account</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>17374d8f-2e2c-4de3-8732-3cd6837baa65</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>f80b7896-78de-4951-8151-4ab2eb570bc6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>17374d8f-2e2c-4de3-8732-3cd6837baa65</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>5e48b486-c49a-4e05-b5c9-75d0e001b46a</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
