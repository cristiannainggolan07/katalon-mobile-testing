<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite - Invalid Account</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>c880be45-6d60-4e87-b034-0c6c2de46eb3</testSuiteGuid>
   <testCaseLink>
      <guid>199fd440-cf91-49d9-9d3d-d19de59b577e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC01 - Login/Login - Invalid Account</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>5c6924e7-f89a-46ff-8edb-29c72a38064b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Invalid Account</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>5c6924e7-f89a-46ff-8edb-29c72a38064b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>02edc68e-ed48-4fcb-bce6-50ff0bcf8f6b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>5c6924e7-f89a-46ff-8edb-29c72a38064b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>4f5ca762-679f-4712-b93f-6119762b090d</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
